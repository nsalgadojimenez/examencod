/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package examen5431;
import java.util.TreeMap;
/**
 * @author NSalgadoJiménez
 */
public class Productos {
    //Las variables de los productos.
    //private int numSerie;
    private String nombre;
    private float precio;
    //
    Productos(){
        TreeMap <String,Float> prod = new TreeMap<>();
            prod.put("Pescado", 5F);
            prod.put("Carne", 10.8F);
            prod.put("Champú", 6.20F);
            prod.put("Amalgama Misteriosa",30F);
            prod.put("Cosas", 3.25F);
    }
    /**
     * @return el nombre.
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * @param nombre introduce el nombre.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     * @return el precio.
     */
    public float getPrecio() {
        return precio;
    }
    /**
     * @param precio introduce el precio.
     */
    public void setPrecio(float precio) {
        this.precio = precio;
    }
}